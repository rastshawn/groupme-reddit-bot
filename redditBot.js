


// edit this line
var botID = "";


var express = require('express');
var app = express();
var fs = require('fs');
var spawn = require('child_process').spawn;
var request = require('request');

app.get('/groupme', function(req, res) {
    console.log(req);
    console.log("get");
});
var i = 0;

app.post('/groupme', function(req, res) {
    var string = "";
    req.on('data', function(data) {
        string += data;
    });

    req.on('end', function(){
        var message = JSON.parse(string);

        // think c++. first command is first word, then so on. 
        var args = message.text.split(" ");

        if (args[0].toLowerCase() == "sos") {
            var sublist = ["aww", "eyebleach", "blep", "puppies"];

            //var sub = sublist[Math.floor(Math.random()*sublist.length)];

            var sub = sublist[i++];
            if (i >= sublist.length) i = 0;
            getNonSticky(1, sub);

        }



    });

});


// because using the /hot endpoint will return stickied posts first. 
// this just posts the first non-sticky post
function getNonSticky (i, subreddit) {

    request({
        url: 'https://reddit.com/r/' + subreddit + '/hot.json?limit=' + i

    }, function(error, response, body) {
        var data = JSON.parse(body).data;
        var content = data.children[i-1].data.url;
        if (data.children[i-1].data.stickied) {
            i++;
            getNonSticky(i, subreddit);
        } else {
            if (content) {
                postToGroup(content);
            } else {
                postToGroup("not an image, sorry");
            }
        }
    });



}


function postToGroup(text) {
                request({
                    url: 'https://api.groupme.com/v3/bots/post',
                    method: "POST",
                    json: true,
                    body : {
                        "bot_id" : botID,
                        "text" : text 
                    }
                },
                  function(error, response, body) {
                    console.log(body);
                    console.log(error);
                  }
                );
}
app.listen(3501, function() {
    console.log('listening on 3501');
});
